module codeberg.org/samwhited/spamblock

go 1.22.0

require github.com/mattn/go-mastodon v0.0.8

require (
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80 // indirect
)

replace github.com/mattn/go-mastodon v0.0.8 => github.com/samwhited/go-mastodon v0.0.0-20241009121915-051b7abc9274
