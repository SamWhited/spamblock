package main

import (
	"context"
	"log"
	"os"
	"strings"

	"github.com/mattn/go-mastodon"
)

var (
	server = os.Getenv("SPAMBLOCK_SERVER")
	tag    = os.Getenv("SPAMBLOCK_TAG")
)

func main() {
	log.Printf("Starting client for %q", server)
	client := mastodon.NewClient(&mastodon.Config{
		Server:       server,
		ClientID:     os.Getenv("SPAMBLOCK_CLIENT_ID"),
		ClientSecret: os.Getenv("SPAMBLOCK_CLIENT_SECRET"),
		AccessToken:  os.Getenv("SPAMBLOCK_ACCESS_TOKEN"),
	})

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ev, err := client.StreamingHashtag(ctx, tag, false)
	if err != nil {
		log.Fatalf("error streaming hash tag: %v", err)
	}
	for e := range ev {
		switch e := e.(type) {
		case *mastodon.UpdateEvent:
			if e.Status == nil {
				// This probably isn't possible, but let's be safe.
				continue
			}
			// TODO: generalize this.
			if !e.Status.Sensitive && strings.HasPrefix(e.Status.Content, `<p><span>はじめまして`) {
				report, err := client.Report(ctx, e.Status.Account.ID, []mastodon.ID{e.Status.ID}, "Automatically reported by Sam's spam bot, please make sure this isn't a false positive before suspending.")
				if err != nil {
					log.Printf("error filing report: %v", err)
				}
				log.Printf("reported %v", report.ID)
			} else {
				log.Printf("Skipped: %v", e.Status.ID)
			}
		case *mastodon.DeleteEvent, *mastodon.UpdateEditEvent, *mastodon.NotificationEvent:
			// Ignore other events.
		case *mastodon.ErrorEvent:
			log.Fatalf("error: %v", e.Err)
		default:
			log.Printf("unknown event %T(%#[1]v)", e)
		}
	}
}
