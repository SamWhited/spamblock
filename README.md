# spamblock

This is a quick script I threw together to help mitigate spam waves on Mastodon.
The current iteration of spam waves are somewhat naive and easy to detect, this
may not always be the case.
This tool does not try to be a generalizable spam system, it is meant to be
simple and block lots of posts that meet obvious and common patterns.


## Building

This project requires [`go-mastodon`] which is, unfortunately, mostly
unmaintained.
The `go.mod` file currently relies on a forked version of it that applies the
patch from [go-mastodon#193].
If you want to use a newer upstream version, you'll need to apply this patch
yourself.

[`go-mastodon`]: https://pkg.go.dev/github.com/mattn/go-mastodon
[go-mastodon#193]: https://github.com/mattn/go-mastodon/pull/193


## Use

`spamblock` can be configured with several environment variables:

```
SPAMBLOCK_SERVER        — The server to connect to, including the scheme, eg.
                          https://social.coop
SPAMBLOCK_TAG           — A hashtag to search for, eg. "SpammersTag"
SPAMBLOCK_CLIENT_ID     — The client ID from the app credentials
SPAMBLOCK_CLIENT_SECRET — The client secret from the app credentials
SPAMBLOCK_ACCESS_TOKEN  — The access token from the app credentials
```


## License

The person who associated a work with this deed has dedicated the work to the
public domain by waiving all of their rights to the work worldwide under
copyright law, including all related and neighboring rights, to the extent
allowed by law.

You can copy, modify, distribute and perform the work, even for commercial
purposes, all without asking permission.

In no way are the patent or trademark rights of any person affected by CC0, nor
are the rights that other persons may have in the work or in how the work is
used, such as publicity or privacy rights.

Unless expressly stated otherwise, the person who associated a work with this
deed makes no warranties about the work, and disclaims liability for all uses of
the work, to the fullest extent permitted by applicable law.

When using or citing the work, you should not imply endorsement by the author or
the affirmer.

See the LICENSE file for the legal code.
